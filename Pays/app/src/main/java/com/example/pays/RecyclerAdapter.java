package com.example.pays;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pays.R;

import com.example.pays.data.Country;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private String[] countries = {"France",
            "Allemagne",
            "Japon",
            "AFS",
            "Espagne",
            "USA"};

    private String[] capitals = {"Paris",
            "Berlin", "Tokyo",
            "Johanesburg", "Madrid",
            "Washington"};

    private int[] flags = { R.drawable.ic_flag_of_france_320px,
            R.drawable.ic_flag_of_germany_320px,
            R.drawable.ic_flag_of_japan_320px,
            R.drawable.ic_flag_of_south_africa_320px,
            R.drawable.ic_flag_of_spain_320px,
            R.drawable.ic_flag_of_the_united_states_320px};

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        viewHolder.itemName.setText(Country.countries[i].getName());
        viewHolder.itemCapital.setText(Country.countries[i].getCapital());

        String uri = Country.countries[i].getImgUri();
        //uri = "@drawable/name_file";
        Context c = viewHolder.itemFlag.getContext();

        viewHolder.itemFlag.setImageDrawable(c.getResources().getDrawable(c.getResources().getIdentifier(uri , null , c.getPackageName())));
    }

    @Override
    public int getItemCount() {
        return countries.length;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView itemFlag;
        TextView itemName;
        TextView itemCapital;

        ViewHolder(View itemView) {
            super(itemView);
            itemFlag = itemView.findViewById(R.id.item_image);
            itemName = itemView.findViewById(R.id.item_title);
            itemCapital = itemView.findViewById(R.id.item_detail);

            int position = getAdapterPosition();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {

                    int position = getAdapterPosition();
                    /* Snackbar.make(v, "Click detected on chapter " + (position+1),
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                     */
                    //// Implementation with bundle
                    // Bundle bundle = new Bundle();
                    // bundle.putInt("numChapter", position);
                    // Navigation.findNavController(v).navigate(R.id.action_FirstFragment_to_SecondFragment, bundle);
                    /*
                    FirstFragmentDirections.ActionFirstFragmentToSecondFragment action = FirstFragmentDirections.actionFirstFragmentToSecondFragment();
                    action.setKeyChapterId(position);
                    Navigation.findNavController(v).navigate(action);

                     */
                }
            });

        }
    }

}