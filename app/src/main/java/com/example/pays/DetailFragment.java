package com.example.pays;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.pays.data.Country;

public class DetailFragment extends Fragment {
    ImageView itemFlag;
    TextView itemName;
    TextView itemCapital;
    TextView itemLangue;
    TextView itemMonnaie;
    TextView itempPop;
    TextView itemSuperficie;
    Context c;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        c = container.getContext();
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DetailFragmentArgs args= DetailFragmentArgs.fromBundle(getArguments());
        int i=args.getCountryId();
        String uri = Country.countries[i].getImgUri();
        itemName=view.findViewById(R.id.Pays);
        itemCapital=view.findViewById(R.id.Capitale);
        itemLangue=view.findViewById(R.id.Langues);
        itemMonnaie=view.findViewById(R.id.Monnaie);
        itempPop=view.findViewById(R.id.Population);
        itemSuperficie=view.findViewById(R.id.Superficie);
        itemFlag=view.findViewById(R.id.Drapeau);

        itemName.setText(Country.countries[i].getName());
        itemFlag.setImageDrawable(c.getResources().getDrawable(c.getResources().getIdentifier(uri, null, c.getPackageName())));
        itemCapital.setText(Country.countries[i].getCapital());
        itemLangue.setText(Country.countries[i].getLanguage());
        itemMonnaie.setText(Country.countries[i].getCurrency());
        itempPop.setText(String.valueOf(Country.countries[i].getPopulation()));
        itemSuperficie.setText(String.valueOf(Country.countries[i].getArea()));

        view.findViewById(R.id.button_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}